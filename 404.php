<?php get_template_part('parts/header'); ?>

<main>
  <?php get_template_part('parts/page', 'header');?>

  <section class="404 padding--bottom">
	<div class="wrap hpad">

    	<p>Vi kunne ikke finde siden du søgte efter.</p>

	</div>
  </section>
  

</main>

<?php get_template_part('parts/footer'); ?>