<?php
/**
 * Description: Lionlab product slider
 *
 * @package Lionlab
 * @subpackage Lionlab
 * @since Version 1.0
 * @author Kaspar Rudbech
*/

if ( have_rows('products') ) : ?>

  <section class="slider slider--products">
    <div class="slider__track--products is-slider is-slider--products">

      <?php
      // Loop through slides
      while ( have_rows('products') ) :
        the_row();
          $image   = get_sub_field('img');
          $title = get_sub_field('title');
          $caption = get_sub_field('text'); 
          $link = get_sub_field('link');
          $link_text = get_sub_field('link_text');
        ?>

        <div class="slider__item slider__item--products flex flex--valign" style="background-image: url(<?php echo esc_url($image['url']);  ?>);">
          <div class="wrap hpad slider__container center">
            <div class="slider__text">
              <h2 class="slider__title"><?php echo $title; ?></h2>
              <p class="slider__caption--products"><?php echo esc_html($caption); ?></p>

              <a class="btn btn--gradient slider__link" href="<?php echo esc_url($link); ?>"><?php echo esc_html($link_text); ?></a> 
            </div>
            <div class="slider__overlay"></div>
          </div>
        </div>

      <?php endwhile; ?>

    </div>
  </section>
<?php endif; ?>