<!doctype html>

<html <?php language_attributes(); ?> class="no-js">

<head>
  <meta charset="utf-8">
  <title><?php wp_title( '|', true, 'right' ); ?></title>
  <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5G68MZP"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>

<div class="transition__pace"></div>

<div id="transition__wrapper">

<div class="transition__container" data-namespace="general">

<?php 
  $adress = get_field('adress', 'options'); 
  $phone = get_field('phone', 'options'); 
  $mail = get_field('mail', 'options'); 
?>

<div class="toolbar orange--bg">
  <div class="wrap hpad flex flex--end">

    <div class="toolbar__item">
      <a href="tel:<?php echo esc_html(get_formatted_phone($phone)); ?>"><?php echo esc_html($phone); ?></a>
    </div>
    <div class="toolbar__item">
      <a href="mailto:<?php echo esc_html($mail); ?>"><?php echo esc_html($mail); ?></a>
    </div>

  </div>
</div>

<header class="header" id="header">
  <div class="wrap hpad flex flex--center flex--justify">

    <a class="header__logo" href="<?php bloginfo('url'); ?>">
      <img src="<?php echo get_template_directory_uri(); ?>/assets/img/logo-white.png" alt="<?php bloginfo('name'); ?>">
    </a>

    <div class="nav-toggle"> 
      <span class="nav-toggle__label"><?php _e('Menu', 'lionlab') ?></span> 
      <span class="nav-toggle__icon"></span>
    </div>

    <nav class="nav" role="navigation">
      <div class="nav--mobile">
        <?php scratch_main_nav(); ?>
      </div>
    </nav>

  </div>
</header>