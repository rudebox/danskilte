<?php 
/**
* Description: Lionlab latest cases field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/


// Custom WP query query
$args_query = array(
  'posts_per_page' => 4,
  'order' => 'DESC'
);

$query = new WP_Query( $args_query );
?>


<?php  if ($query->have_posts() ) : ?>
<section class="cases">
  <div class="wrap--fluid">
    <div class="flex flex--wrap cases__row">

      <?php while ($query->have_posts() ) : $query->the_post(); ?>
        <?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'url' );?>

        <a href="<?php the_permalink(); ?>" class="col-sm-3 cases__item" style="background-image: url(<?php echo esc_url($thumb[0]); ?>);" itemscope itemtype="http://schema.org/BlogPosting">

          <header class="cases__header">

            <?php foreach ((get_the_category()) as $category) : ?>
              <h6 class="cases__cat"><?php echo $category->name; ?></h6>
            <?php endforeach; ?>

            <h3 class="cases__title" itemprop="headline"><?php the_title(); ?></h3>

          </header>

          <span class="cases__btn">se case <i class="fas fa-angle-right"></i></span>
          
        </a>

      <?php endwhile; ?>

      <?php wp_reset_postdata(); ?>

    </div>
  </div>
</section>
<?php endif; ?>