<?php 
/**
* Description: Lionlab text-img field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

//sections settings
$bg = get_sub_field('bg');
$margin = get_sub_field('margin');
$title = get_sub_field('header');
$position = get_sub_field('position');
$text = get_sub_field('text');
$img = get_sub_field('img');
$class = get_sub_field('class');

if ($position === 'right') {
	$pull = 'col-sm-pull-6';
	$push = 'col-sm-push-6';
}

else {
	$pull = '';
	$push = '';
}

?>

<section class="text-img <?php echo esc_attr($bg); ?>--bg padding--<?php echo esc_attr($margin); ?> <?php echo esc_attr($class); ?>">
	<div class="wrap hpad">
		<div class="row flex flex--wrap text-img__row">
			<div class="col-sm-6 text-img__text <?php echo esc_attr($push); ?>">
				<?php echo $text; ?>
			</div>
			<div class="col-sm-6 text-img__img <?php echo esc_attr($pull); ?>" style="background-image: url(<?php echo $img['url']; ?>);">
			</div>
		</div>
	</div>
</section>
