<?php
if( have_rows('layout') ) {
  $GLOBALS['layout_count'] = 1; while ( have_rows('layout') ) { the_row();
    if( get_row_layout() === 'hero_unit' ) { ?>

      <?php get_template_part( 'parts/layouts/layout', 'hero_unit' ); ?>

    <?php
    } elseif( get_row_layout() === 'flexible_columns' ) {
    ?>

      <?php get_template_part( 'parts/layouts/layout', 'flexible_columns' ); ?>

    <?php
    } elseif( get_row_layout() === 'staggered_images_with_text' ) {
    ?>

      <?php get_template_part( 'parts/layouts/layout', 'staggered_images_with_text' ); ?>

    <?php
    } elseif( get_row_layout() === 'slider' ) {
    ?>

      <?php get_template_part( 'parts/layouts/layout', 'slider' ); ?>

    <?php
    } elseif( get_row_layout() === 'link-boxes' ) {
    ?>

      <?php get_template_part( 'parts/layouts/layout', 'link-boxes' ); ?>

    <?php
    } elseif( get_row_layout() === 'product-slider' ) {
    ?>

      <?php get_template_part( 'parts/layouts/layout', 'product-slider' ); ?>

    <?php
    } elseif( get_row_layout() === 'offers' ) {
    ?>

      <?php get_template_part( 'parts/layouts/layout', 'offers' ); ?>

    <?php
    } elseif( get_row_layout() === 'gallery' ) {
    ?>

      <?php get_template_part( 'parts/layouts/layout', 'gallery' ); ?>

    <?php
    } elseif( get_row_layout() === 'text-img' ) {
    ?>

      <?php get_template_part( 'parts/layouts/layout', 'text-img' ); ?>

    <?php
    } elseif( get_row_layout() === 'intro' ) {
    ?>

      <?php get_template_part( 'parts/layouts/layout', 'intro' ); ?>

    <?php
    } elseif( get_row_layout() === 'wysiwygs' ) {
    ?>

      <?php get_template_part( 'parts/layouts/layout', 'wysiwygs' ); ?>

    <?php
    }
    ?>

  <?php
    $GLOBALS['layout_count']++;
  }
  ?>

<?php
} else {
?>
  <p class="center">You haven't added any layouts yet. <?php edit_post_link('Add one now.'); ?></p>
<?php
}
?>
