<?php 
/**
* Description: Lionlab link-boxes repeater field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

//sections settings
$bg = get_sub_field('bg');
$margin = get_sub_field('margin');
$title = get_sub_field('header');
$main_link = get_sub_field('link');
$link_text = get_sub_field('link_text');

if (have_rows('linkbox') ) :
?>

<section class="link-boxes <?php echo $bg; ?>--bg padding--<?php echo $margin; ?>">
	<div class="wrap hpad link-boxes__container">

		<h2 class="link-boxes__header center"><?php echo esc_html($title); ?></h2>

		<div class="row flex flex--wrap">
			<?php while (have_rows('linkbox') ) : the_row(); 
				$title = get_sub_field('title');
				$text = get_sub_field('text');
				$icon = get_sub_field('icon');
				$link = get_sub_field('link');
			?>

			<a href="<?php echo esc_url($link); ?>" class="col-sm-6 col-md-3 link-boxes__item wow fadeInUp">
				<?php if ($icon) : ?> 
				<div class="link-boxes__wrap">
					<img class="link-boxes__img" src="<?php echo esc_url($icon['sizes']['offer']); ?>" alt="<?php echo esc_attr($icon['alt']); ?>">
				</div>
				<?php endif; ?>
				<h3 class="link-boxes__title"><?php echo esc_html($title); ?></h3> 
				<?php echo esc_html($text); ?>
				<div class="link-boxes__btn">Læs mere</div>
			</a>
			<?php endwhile; ?>
		</div>
		
		<div class="center">
			<a class="link-boxes__link btn btn--orange wow fadeIn" data-wow-delay=".3s" href="<?php echo esc_url($main_link); ?>"><span></span><?php echo esc_html($link_text); ?></a>
		</div>

	</div>
</section>
<?php endif; ?>