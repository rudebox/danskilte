<?php get_template_part('parts/header'); ?>

<main>

  <?php get_template_part('parts/content', 'layouts'); ?>
  <?php get_template_part('parts/latest', 'cases'); ?>
  <?php get_template_part('parts/fb', 'feed'); ?>
  <?php get_template_part('parts/references'); ?>

</main>

<?php get_template_part('parts/footer'); ?>
