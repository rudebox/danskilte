<?php 
/**
* Description: Lionlab gallery field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

//gallery
$gallery = get_field('gallery');

if ( $gallery ) : ?>

	<section class="gallery padding--bottom">
		<div class="wrap hpad">
			<h2 class="gallery__title">Galleri</h2>

			<div class="row gallery__list flex flex--wrap"> 

				<?php
					// Loop through gallery
					foreach ( $gallery as $image ) : 
				?>


					<div class="gallery__item" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
						<a href="<?= $image['sizes']['large']; ?>" class="js-zoom gallery__link no-ajax" data-fancybox="gallery-<?= $index; ?>" itemprop="contentUrl" data-caption="<?= $image['caption']; ?>" title="<?= $image['title'] ?>" data-width="<?= $image['sizes']['large-width']; ?>" data-height="<?= $image['sizes']['large-height']; ?>">
							<img class="gallery__image" data-src="<?= $image['sizes']['offer']; ?>" src="<?= $image['sizes']['offer']; ?>" alt="<?= $image['alt']; ?>" itemprop="thumbnail" height="<?= $image['sizes']['large-height']; ?>" width="<?= $image['sizes']['large-width']; ?>">
						</a>
					</div>
				<?php endforeach; ?>

			</div>
		</div>
	</section>

<?php endif; ?>